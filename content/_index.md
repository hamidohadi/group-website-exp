---
# Leave the homepage title empty to use the site title
title:
date: 2022-10-24
type: landing

sections:
  - block: hero
    content:
      title: Quantum Light-Matter
      cta_note:
        label: 
      text: |-
        ## University of St Andrews
        ### We are an experimental group at [School of Physics and Astronomy](https://www.st-andrews.ac.uk/physics-astronomy/)) working on strong light-matter interaction, specifically [exciton-polaritons](https://www.youtube.com/watch?v=sWmvZ0IGrsU).

    design:
      background:
        image: 
          filename: topbg.png
        image_darken: 0.7
        #  Options are `cover` (default), `contain`, or `actual` size.
        image_size: cover
        # Options include `left`, `center` (default), or `right`.
        image_position: center
        # Use a fun parallax-like fixed background effect on desktop? true/false
        image_parallax: true
        # Text color (true=light, false=dark, or remove for the dynamic theme color).
        text_color_light: true

  - block: collection
    id: posts
    content:
      title: Recent News
      subtitle: ''
      text: ''
      # Choose how many pages you would like to display (0 = all pages)
      count: 2
      # Filter on criteria
      filters:
        folders:
          - post
        author: ""
        category: ""
        tag: ""
        exclude_featured: false
        exclude_future: false
        exclude_past: false
        publication_type: ""
      # Choose how many pages you would like to offset by
      offset: 0
      # Page order: descending (desc) or ascending (asc) date.
      order: desc
    design:
      # Choose a layout view
      view: compact
      columns: '2'
  - block: portfolio
    id: projects
    content:
      title: Projects
      filters:
        folders:
          - project
      # Default filter index (e.g. 0 corresponds to the first `filter_button` instance below).
      default_button_index: 0
      # Filter toolbar (optional).
      # Add or remove as many filters (`filter_button` instances) as you like.
      # To show all items, set `tag` to "*".
      # To filter by a specific tag, set `tag` to an existing tag name.
      # To remove the toolbar, delete the entire `filter_button` block.
      # buttons:
      #   - name: All
      #     tag: '*'
      #   - name: Deep Learning
      #     tag: Deep Learning
      #   - name: Other
      #     tag: Demo
    design:
      # Choose how many columns the section has. Valid values: '1' or '2'.
      columns: '2'
      view: showcase
      # For Showcase view, flip alternate rows?
      flip_alt_rows: true
  # - block: markdown
  #   content:
  #     title: Gallery
  #     subtitle: ''
  #     text: |-
  #       {{< gallery album="demo" >}}
  #   design:
  #     columns: '1'
  - block: collection
    id: publications
    content:
      title: Recent Publications
      text: |-
        {{% callout note %}}
        Quickly discover relevant content by [filtering publications](./publication/).
        {{% /callout %}}
      filters:
        folders:
          - publication
        exclude_featured: true
    design:
      columns: '2'
      view: citation
  # - block: collection
  #   id: talks
  #   content:
  #     title: Recent & Upcoming Talks
  #     filters:
  #       folders:
  #         - event
  #   design:
  #     columns: '2'
  #     view: compact
  - block: contact
    id: contact
    content:
      title: Contact
      subtitle:
      # text: |-
      #   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam mi diam, venenatis ut magna et, vehicula efficitur enim.
      # Contact (add or remove contact options as necessary)
      email: ho35@st-andrews.ac.uk
      address:
        street: Room 332, Physics and Astronomy, North Haugh
        city: St Andrews
        region: United Kingdom
        postcode: 'KY16 9SS'
        country: United Kingdom
        country_code: UK
      # directions: Enter Building 1 and take the stairs to Office 200 on Floor 2
      # contact_links:
      #   - icon: twitter
      #     icon_pack: fab
      #     name: 'DM'
      #     link: 'https://twitter.com/FluidsOfLight'
      # Automatically link email and phone or display as text?
      autolink: true
      # Email form provider
      coordinates:
        latitude: '56.34017'
        longitude: '-2.80661'
      
    design:
      columns: '2'
---
