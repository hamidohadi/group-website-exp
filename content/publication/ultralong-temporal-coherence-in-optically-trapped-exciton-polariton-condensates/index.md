---
title: Ultralong temporal coherence in optically trapped exciton-polariton condensates
publication_types:
  - "0"
  - "2"
authors:
  - K. Orfanakis
  - A. F. Tzortzakakis
  - D. Petrosyan
  - P. G. Savvidis
  - and H. Ohadi
doi: 10.1103/PhysRevB.103.235313
publication: "*Physical Review B*"
publication_short: "*Phys. Rev. B*"
abstract: We investigate an optically trapped exciton-polariton condensate and
  observe temporal coherence beyond 1 ns in duration. Due to the reduction of
  the spatial overlap with the thermal reservoir of excitons, the coherence time
  of the trapped condensate is more than an order of magnitude longer than that
  of an untrapped condensate. This ultralong coherence enables high-precision
  spectroscopy of the trapped condensate, and we observe periodic beats of the
  field correlation function due to a fine energy splitting of two polarization
  modes of the condensate. Our results are important for realizing polariton
  simulators with spinor condensates in lattice potentials.
draft: false
featured: false
image:
  filename: ""
  focal_point: Smart
  preview_only: false
date: 2021-06-22T13:17:33.117Z
---
