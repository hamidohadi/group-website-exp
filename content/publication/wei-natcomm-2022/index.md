---
# Documentation: https://wowchemy.com/docs/managing-content/
title: Optically trapped room temperature polariton condensate in an organic semiconductor
publication_types:
  - "2"
authors:
  - Mengjie Wei
  - Wouter Verstraelen
  - Konstantinos Orfanakis
  - Arvydas Ruseckas
  - Timothy C. H. Liew
  - Ifor D.W. Samuel
  - Graham A. Turnbull
  - Hamid Ohadi
doi: 10.1038/s41467-022-34440-0
publication: "*Nature Communications*"
publication_short: "*Nature Comm.*"
abstract: The strong nonlinearities of exciton-polariton condensates in lattices make them suitable candidates for neuromorphic computing and physical simulations of complex problems. So far, all room temperature polariton condensate lattices have been achieved by nanoimprinting microcavities, which by nature lacks the crucial tunability required for realistic reconfigurable simulators. Here, we report the observation of a quantised oscillating nonlinear quantum fluid in 1D and 2D potentials in an organic microcavity at room temperature, achieved by an on-the-fly fully tuneable optical approach. Remarkably, the condensate is delocalised from the excitation region by macroscopic distances, leading both to longer coherence and a threshold one order of magnitude lower than that with a conventional Gaussian excitation profile. We observe different mode selection behaviour compared to inorganic materials, which highlights the anomalous scaling of blueshift with pump intensity and the presence of sizeable energy-relaxation mechanisms. Our work is a major step towards a fully tuneable polariton simulator at room temperature.
draft: false
featured: false
tags:
  - polariton
image:
  filename: featured
  focal_point: Smart
  preview_only: false
date: 2022-12-05T11:27:44.188Z
---