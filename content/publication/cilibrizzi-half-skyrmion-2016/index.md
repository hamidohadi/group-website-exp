---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Half-skyrmion spin textures in polariton microcavities
subtitle: ''
summary: ''
authors:
- P. Cilibrizzi
- H. Sigurdsson
- T. C. H. Liew
- H. Ohadi
- A. Askitopoulos
- S. Brodbeck
- C. Schneider
- I. A. Shelykh
- S. Höfling
- J. Ruostekoski
- P. Lagoudakis
tags: []
categories: []
date: '2016-07-01'
lastmod: 2020-12-28T15:15:07Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:54.997676Z'
publication_types:
- '2'
abstract: We study the polarization dynamics of a spatially expanding polariton condensate
  under nonresonant linearly polarized optical excitation. The spatially and temporally
  resolved polariton emission reveals the formation of nontrivial spin textures in
  the form of a quadruplet polarization pattern both in the linear and circular Stokes
  parameters, and an octuplet in the diagonal Stokes parameter. The continuous rotation
  of the polariton pseudospin vector through the condensate due to TE-TM splitting
  exhibits an ordered pattern of half-skyrmions associated with a half-integer topological
  number. A theoretical model based on a driven-dissipative Gross-Pitaevskii equation
  coupled with an exciton reservoir describes the dynamics of the nontrivial spin
  textures through the optical spin-Hall effect.
publication: '*Physical Review B*'
url_pdf: http://link.aps.org/doi/10.1103/PhysRevB.94.045315
doi: 10.1103/PhysRevB.94.045315
---
