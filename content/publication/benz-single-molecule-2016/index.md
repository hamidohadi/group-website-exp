---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Single-molecule optomechanics in “picocavities”
subtitle: ''
summary: ''
authors:
- Felix Benz
- Mikolaj K. Schmidt
- Alexander Dreismann
- Rohit Chikkaraddy
- Yao Zhang
- Angela Demetriadou
- Cloudy Carnegie
- Hamid Ohadi
- Bart de Nijs
- Ruben Esteban
- Javier Aizpurua
- Jeremy J. Baumberg
tags: []
categories: []
date: '2016-11-01'
lastmod: 2020-12-28T15:15:07Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:55.533707Z'
publication_types:
- '2'
abstract: A cool route to nanospectroscopy Confining light to a cavity is often used
  to enhance the interaction between the light and a particle stored within the cavity.
  Benz et al. worked with a self-assembled monolayer of biphenyl-4-thiol molecules
  sandwiched between a gold film and a gold nanoparticle. They used laser irradiation
  to move atoms in the nanoparticle and produced a “picocavity” that was stable at
  cryogenic temperatures. The authors were then able to obtain time-dependent Raman
  spectra from individual molecules. Such subwavelength cavities that can localize
  light to volumes well below 1 nm3 will enable optical experiments on the atomic
  scale. Science, this issue p. 726 Trapping light with noble metal nanostructures
  overcomes the diffraction limit and can confine light to volumes typically on the
  order of 30 cubic nanometers. We found that individual atomic features inside the
  gap of a plasmonic nanoassembly can localize light to volumes well below 1 cubic
  nanometer (“picocavities”), enabling optical experiments on the atomic scale. These
  atomic features are dynamically formed and disassembled by laser irradiation. Although
  unstable at room temperature, picocavities can be stabilized at cryogenic temperatures,
  allowing single atomic cavities to be probed for many minutes. Unlike traditional
  optomechanical resonators, such extreme optical confinement yields a factor of 106
  enhancement of optomechanical coupling between the picocavity field and vibrations
  of individual molecular bonds. This work sets the basis for developing nanoscale
  nonlinear quantum optics on the single-molecule level. Strongly subwavelength optical
  cavities can be used to spectroscopically probe single molecules. Strongly subwavelength
  optical cavities can be used to spectroscopically probe single molecules.
publication: '*Science*'
url_pdf: http://science.sciencemag.org/content/354/6313/726
doi: 10.1126/science.aah5243
---
