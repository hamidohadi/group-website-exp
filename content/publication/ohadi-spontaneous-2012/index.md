---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Spontaneous Symmetry Breaking in a Polariton and Photon Laser
subtitle: ''
summary: ''
authors:
- H. Ohadi
- E. Kammann
- T. C. H. Liew
- K. G. Lagoudakis
- A. V. Kavokin
- P. G. Lagoudakis
tags: []
categories: []
date: '2012-07-01'
lastmod: 2020-12-28T15:15:09Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:52.561730Z'
publication_types:
- '2'
abstract: We report on the simultaneous observation of spontaneous symmetry breaking
  and long-range spatial coherence both in the strong- and the weak-coupling regime
  in a semiconductor microcavity. Under pulsed excitation, the formation of a stochastic
  order parameter is observed in polariton and photon lasing regimes. Single-shot
  measurements of the Stokes vector of the emission exhibit the buildup of stochastic
  polarization. Below threshold, the polarization noise does not exceed 10%, while
  above threshold we observe a total polarization of up to 50% after each excitation
  pulse, while the polarization averaged over the ensemble of pulses remains nearly
  zero. In both polariton and photon lasing regimes, the stochastic polarization buildup
  is accompanied by the buildup of spatial coherence. We find that the Landau criterion
  of spontaneous symmetry breaking and Penrose-Onsager criterion of long-range order
  for Bose-Einstein condensation are met in both polariton and photon lasing regimes.
publication: '*Physical Review Letters*'
url_pdf: http://link.aps.org/doi/10.1103/PhysRevLett.109.016404
doi: 10.1103/PhysRevLett.109.016404
---
