---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Strain-assisted optomechanical coupling of polariton condensate spin to a micromechanical
  resonator
subtitle: ''
summary: ''
authors:
- O. Be'er
- H. Ohadi
- Y. del Valle-Inclan Redondo
- A. J. Ramsay
- S. I. Tsintzos
- Z. Hatzopoulos
- P. G. Savvidis
- J. J. Baumberg
tags: []
categories: []
date: '2017-12-01'
lastmod: 2020-12-28T15:15:06Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:56.057001Z'
publication_types:
- '2'
abstract: We report spin and intensity coupling of an exciton-polariton condensate
  to the mechanical vibrations of a circular membrane microcavity. We optically drive
  the microcavity resonator at the lowest mechanical resonance frequency while creating
  an optically trapped spin-polarized polariton condensate in different locations
  on the microcavity and observe spin and intensity oscillations of the condensate
  at the vibration frequency of the resonator. Spin oscillations are induced by vibrational
  strain driving, whilst the modulation of the optical trap due to the displacement
  of the membrane causes intensity oscillations in the condensate emission. Our results
  demonstrate spin-phonon coupling in a macroscopically coherent condensate.
publication: '*Applied Physics Letters*'
url_pdf: http://aip.scitation.org/doi/full/10.1063/1.5011719
doi: 10.1063/1.5011719
---
