---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Driven-dissipative spin chain model based on exciton-polariton condensates
subtitle: ''
summary: ''
authors:
- H. Sigurdsson
- A. J. Ramsay
- H. Ohadi
- Y. G. Rubo
- T. C. H. Liew
- J. J. Baumberg
- I. A. Shelykh
tags: []
categories: []
date: '2017-10-01'
lastmod: 2020-12-28T15:15:11Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:55.863053Z'
publication_types:
- '2'
abstract: An infinite chain of driven-dissipative condensate spins with uniform nearest-neighbor
  coherent coupling is solved analytically and investigated numerically. Above a critical
  occupation threshold the condensates undergo spontaneous spin bifurcation (becoming
  magnetized) forming a binary chain of spin-up or spin-down states. Minimization
  of the bifurcation threshold determines the magnetic order as a function of the
  coupling strength. This allows control of multiple magnetic orders via adiabatic
  (slow ramping of) pumping. In addition to ferromagnetic and antiferromagnetic ordered
  states we show the formation of a paired-spin ordered state textbar?textuparrow?textuparrow??textdownarrow?textdownarrow??
  as a consequence of the phase degree of freedom between condensates.
publication: '*Physical Review B*'
url_pdf: https://link.aps.org/doi/10.1103/PhysRevB.96.155403
doi: 10.1103/PhysRevB.96.155403
---
