---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Synchronization crossover of polariton condensates in weakly disordered lattices
subtitle: ''
summary: ''
authors:
- H. Ohadi
- Y. del Valle-Inclan Redondo
- A. J. Ramsay
- Z. Hatzopoulos
- T. C. H. Liew
- P. R. Eastham
- P. G. Savvidis
- J. J. Baumberg
tags: []
categories: []
date: '2018-05-01'
lastmod: 2020-12-28T15:15:10Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:56.244884Z'
publication_types:
- '2'
abstract: We demonstrate that the synchronization of a lattice of solid-state condensates
  when intersite tunneling is switched on depends strongly on the weak local disorder.
  This finding is vital for implementation of condensate arrays as computation devices.
  The condensates here are nonlinear bosonic fluids of exciton-polaritons trapped
  in a weakly disordered Bose-Hubbard potential, where the nearest-neighboring tunneling
  rate (Josephson coupling) can be dynamically tuned. The system can thus be tuned
  from a localized to a delocalized fluid as the number density or the Josephson coupling
  between nearest neighbors increases. The localized fluid is observed as a lattice
  of unsynchronized condensates emitting at different energies set by the disorder
  potential. In the delocalized phase, the condensates synchronize and long-range
  order appears, evidenced by narrowing of momentum and energy distributions, new
  diffraction peaks in momentum space, and spatial coherence between condensates.
  Our paper identifies similarities and differences of this nonequilibrium crossover
  to the traditional Bose-glass to superfluid transition in atomic condensates.
publication: '*Physical Review B*'
url_pdf: https://link.aps.org/doi/10.1103/PhysRevB.97.195109
doi: 10.1103/PhysRevB.97.195109
---
