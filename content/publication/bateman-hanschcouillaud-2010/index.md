---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Hänsch–Couillaud locking of Mach?Zehnder interferometer for carrier removal
  from a phase-modulated optical spectrum
subtitle: ''
summary: ''
authors:
- J. E. Bateman
- R. L. D. Murray
- M. Himsworth
- H. Ohadi
- A. Xuereb
- T. Freegarde
tags:
- '"Frequency filtering"'
- '"Phase modulation"'
categories: []
date: '2010-08-01'
lastmod: 2020-12-28T15:15:06Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:53.274480Z'
publication_types:
- '2'
abstract: We describe and analyze the operation and stabilization of a Mach–Zehnder
  interferometer, which separates the carrier and the first-order sidebands of a phase-modulated
  laser field, and which is locked using the Hänsch–Couillaud method. In addition
  to the necessary attenuation, our interferometer introduces, via total internal
  reflection, a significant polarization-dependent phase delay. We employ a general
  treatment to describe an interferometer with an object that affects the field along
  one path, and we examine how this phase delay affects the error signal. We discuss
  the requirements necessary to ensure the lock point remains unchanged when phase
  modulation is introduced, and we demonstrate and characterize this locking experimentally.
  Finally, we suggest an extension to this locking strategy using heterodyne detection.
publication: '*Journal of the Optical Society of America B*'
url_pdf: http://josab.osa.org/abstract.cfm?URI=josab-27-8-1530
doi: 10.1364/JOSAB.27.001530
---
