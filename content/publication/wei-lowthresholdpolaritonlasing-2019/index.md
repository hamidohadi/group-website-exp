---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Low-threshold polariton lasing in a highly disordered conjugated polymer
subtitle: ''
summary: ''
authors:
- Mengjie Wei
- Sai Kiran Rajendran
- Hamid Ohadi
- Laura Tropf
- Malte C. Gather
- Graham A. Turnbull
- Ifor D. W. Samuel
tags:
- '"Dielectric microcavities"'
- '"Infrared radiation"'
- '"Integrating spheres"'
- '"Laser beams"'
- '"Organic light emitting diodes"'
- '"Visible light"'
categories: []
date: '2019-09-01'
lastmod: 2020-12-28T15:15:11Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
publishDate: '2020-12-31T22:03:57.327563Z'
publication_types:
- '2'
abstract: Low-threshold, room-temperature polariton lasing is crucial for future application
  of polaritonic devices. Conjugated polymers are attractive candidates for room-temperature
  polariton lasers, due to their high exciton binding energy, very high oscillator
  strength, easy fabrication, and tunability. However, to date, polariton lasing has
  only been reported in one conjugated polymer, ladder-type MeLPPP, whose very rigid
  structure gives an atypically narrow excitonic linewidth. Here, we observe polariton
  lasing in a highly disordered prototypical conjugated polymer, poly(9,9-dioctylfluorene),
  thereby opening up the field of polymer materials for polaritonics. The long-range
  spatial coherence of the emission shows a maximum fringe visibility contrast of
  72%. The observed polariton lasing threshold (27.7&#x2009;&#x2009;&#x03BC;J/cm2,
  corresponding to an absorbed pump fluence of 19.1&#x2009;&#x2009;&#x03BC;J/cm2)
  is an order of magnitude smaller than for the previous polymer polariton laser,
  potentially bringing electrical pumping of such devices a step closer.
publication: '*Optica*'
url_pdf: https://www.osapublishing.org/optica/abstract.cfm?uri=optica-6-9-1124
doi: 10.1364/OPTICA.6.001124
---
