---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Observation of inversion, hysteresis, and collapse of spin in optically trapped
  polariton condensates
subtitle: ''
summary: ''
authors:
- Yago del Valle-Inclan Redondo
- Helgi Sigurdsson
- Hamid Ohadi
- Ivan A. Shelykh
- Yuri G. Rubo
- Zacharias Hatzopoulos
- Pavlos G. Savvidis
- Jeremy J. Baumberg
tags: []
categories: []
date: '2019-04-01'
lastmod: 2020-12-28T15:15:08Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:57.155724Z'
publication_types:
- '2'
abstract: 'The spin and intensity of optically trapped polariton condensates are studied
  under steady-state elliptically polarized nonresonant pumping. Three distinct effects
  are observed: (1) spin inversion where condensation occurs in the opposite handedness
  from the pump, (2) spin and intensity hysteresis as the pump power is scanned, and
  (3) a sharp “spin collapse” transition in the condensate spin as a function of the
  pump ellipticity. We show these effects are strongly dependent on trap size and
  sample position and are linked to small counterintuitive energy differences between
  the condensate spin components. Our results, which fail to be fully described within
  the commonly used nonlinear equations for polariton condensates, show that a more
  accurate microscopic picture is needed to unify these phenomena in a two-dimensional
  condensate theory.'
publication: '*Physical Review B*'
url_pdf: https://link.aps.org/doi/10.1103/PhysRevB.99.165311
doi: 10.1103/PhysRevB.99.165311
---
