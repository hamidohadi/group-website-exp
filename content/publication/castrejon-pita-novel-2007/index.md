---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Novel designs for Penning ion traps
subtitle: ''
summary: ''
authors:
- J. R. Castrejón-Pita
- H. Ohadi
- D. R. Crick
- D. F. A. Winters
- D. M. Segal
- R. C. Thompson
tags: []
categories: []
date: '2007-01-01'
lastmod: 2020-12-28T15:15:07Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:52.060025Z'
publication_types:
- '2'
abstract: A number of alternative designs are presented for Penning ion traps suitable
  for quantum information processing (QIP) applications with atomic ions. The first
  trap design is a simple array of long straight wires, which allows easy optical
  access. A prototype of this trap has been built to trap Ca+ and a simple electronic
  detection scheme has been employed to demonstrate the operation of the trap. Another
  trap design consists of a conducting plate with a hole in it situated above a continuous
  conducting plane. The final trap design is based on an array of pad electrodes.
  Although this trap design lacks the open geometry of the other traps described above,
  the pad design may prove useful in a hybrid scheme in which information processing
  and qubit storage take place in different types of trap. The behaviour of the pad
  traps is simulated numerically and techniques for moving ions rapidly between traps
  are discussed. Future experiments with these various designs are discussed. All
  of the designs lend themselves to the construction of multiple trap arrays, as required
  for scalable ion trap QIP.
publication: '*Journal of Modern Optics*'
url_pdf: http://www.tandfonline.com/doi/abs/10.1080/09500340600736793
doi: 10.1080/09500340600736793
---
