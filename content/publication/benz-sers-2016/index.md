---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'SERS of Individual Nanoparticles on a Mirror: Size Does Matter, but so Does
  Shape'
subtitle: ''
summary: ''
authors:
- Felix Benz
- Rohit Chikkaraddy
- Andrew Salmon
- Hamid Ohadi
- Bart de Nijs
- Jan Mertens
- Cloudy Carnegie
- Richard W. Bowman
- Jeremy J. Baumberg
tags: []
categories: []
date: '2016-06-01'
lastmod: 2020-12-28T15:15:07Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:54.809116Z'
publication_types:
- '2'
abstract: Coupling noble metal nanoparticles by a 1 nm gap to an underlying gold mirror
  confines light to extremely small volumes, useful for sensing on the nanoscale.
  Individually measuring 10 000 of such gold nanoparticles of increasing size dramatically
  shows the different scaling of their optical scattering (far-field) and surface-enhanced
  Raman emission (SERS, near-field). Linear red-shifts of the coupled plasmon modes
  are seen with increasing size, matching theory. The total SERS from the few hundred
  molecules under each nanoparticle dramatically increases with increasing size. This
  scaling shows that maximum SERS emission is always produced from the largest nanoparticles,
  irrespective of tuning to any plasmonic resonances. Changes of particle facet with
  nanoparticle size result in vastly weaker scaling of the near-field SERS, without
  much modifying the far-field, and allows simple approaches for optimizing practical
  sensing.
publication: '*The Journal of Physical Chemistry Letters*'
url_pdf: http://dx.doi.org/10.1021/acs.jpclett.6b00986
doi: 10.1021/acs.jpclett.6b00986
---
