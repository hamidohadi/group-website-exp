---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Two-ion Coulomb crystals of Ca+ in a Penning trap
subtitle: ''
summary: ''
authors:
- D. R. Crick
- H. Ohadi
- I. Bhatti
- R. C. Thompson
- D. M. Segal
tags:
- '"Laser cooling"'
- '"Quantum information and processing"'
categories: []
date: '2008-02-01'
lastmod: 2020-12-28T15:15:08Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:52.402493Z'
publication_types:
- '2'
abstract: Results demonstrating laser cooling and observation of individual calcium
  ions in a Penning trap are presented. We show that we are able to trap, cool, image
  and manipulate the shape of very small ensembles of ions sufficiently well to produce
  two-ion textquoteleftCoulomb crystalstextquoteright aligned along the magnetic field
  of a Penning trap. Images are presented which show the individual ions to be resolved
  in a two-ion crystal. A distinct change in the configuration of such a crystal is
  observed as the experimental parameters are changed. These structures could eventually
  be used as building blocks in a Penning trap based quantum computer.
publication: '*Optics Express*'
url_pdf: http://www.opticsexpress.org/abstract.cfm?URI=oe-16-4-2351
doi: 10.1364/OE.16.002351
---
