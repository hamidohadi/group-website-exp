---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Stochastic spin flips in polariton condensates: nonlinear tuning from GHz
  to sub-Hz'
subtitle: ''
summary: ''
authors:
- Yago del Valle-Inclan Redondo
- Hamid Ohadi
- Yuri G. Rubo
- Orr Beer
- Andrew J. Ramsay
- Symeon I. Tsintzos
- Zacharias Hatzopoulos
- Pavlos G. Savvidis
- Jeremy J. Baumberg
tags: []
categories: []
date: '2018-01-01'
lastmod: 2020-12-28T15:15:11Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:56.432618Z'
publication_types:
- '2'
abstract: The stability of spin of macroscopic quantum states to intrinsic noise is
  studied for non-resonantly-pumped optically-trapped polariton condensates. We demonstrate
  flipping between the two spin-polarised states with textgreater10 4 slow-down of
  the flip rate by tuning the optical pump power. Individual spin flips faster than
  50 ps are time resolved using single-shot streak camera imaging. We reproduce our
  results within a mean-field model accounting for cross-spin scattering between excitons
  and polaritons, yielding a ratio of cross- to co-spin scattering of ̃0.6, in contrast
  with previous literature suggestions.
publication: '*New Journal of Physics*'
url_pdf: http://stacks.iop.org/1367-2630/20/i=7/a=075008
doi: 10.1088/1367-2630/aad377
---
