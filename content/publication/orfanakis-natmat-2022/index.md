---
title: Rydberg exciton–polaritons in a Cu2O microcavity1
publication_types:
  - "2"
authors:
  - Konstantinos Orfanakis
  - Sai Kiran Rajendran
  - Valentin Walther
  - Thomas Volz
  - Thomas Pohl
  - Hamid Ohadi
doi: 10.1038/s41563-022-01230-4
publication: "*Nature Materials*"
publication_short: "*Nature Mat.*"
abstract: Giant Rydberg excitons with principal quantum numbers as high as n = 25 have been observed in cuprous oxide (Cu2O), a semiconductor in which the exciton diameter can become as large as ∼1 μm. The giant dimension of these excitons results in excitonic interaction enhancements of orders of magnitude. Rydberg exciton–polaritons, formed by the strong coupling of Rydberg excitons to cavity photons, are a promising route to exploit these interactions and achieve a scalable, strongly correlated solid-state platform. However, the strong coupling of these excitons to cavity photons has remained elusive. Here, by embedding a thin Cu2O crystal into a Fabry–Pérot microcavity, we achieve strong coupling of light to Cu2O Rydberg excitons up to n = 6 and demonstrate the formation of Cu2O Rydberg exciton–polaritons. These results pave the way towards realizing strongly interacting exciton–polaritons and exploring strongly correlated phases of matter using light on a chip.
draft: false
featured: false
tags:
  - polariton
image:
  filename: featured
  focal_point: Smart
  preview_only: false
date: 2022-06-21T11:27:44.188Z
---
