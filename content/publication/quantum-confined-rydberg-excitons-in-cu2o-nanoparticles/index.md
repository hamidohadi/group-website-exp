---
title: Quantum confined Rydberg excitons in  Cu2O  nanoparticles
publication_types:
  - "0"
  - "2"
authors:
  - Konstantinos Orfanakis
  - Sai Kiran Rajendran
  - Hamid Ohadi
  - Sylwia Zielińska-Raczyńska
  - Gerard Czajkowski
  - Karol Karpiński
  - and David Ziemkiewicz
doi: 10.1103/PhysRevB.103.245426
publication: "*Physical Review B*"
publication_short: "*Phys. Rev. B*"
abstract: >-
  The quantum confinement of Rydberg excitons is an important step towards
  exploiting their large nonlinearities 

  for quantum applications. We observe Rydberg excitons in natural nanoparticles of Cu$_2$O . We resolve up to
   the principal quantum number $n=12$ in a bulk Cu$_2$O  crystal and up to $n=6$ in nanoparticles extracted 
  from the same crystal. The exciton transitions in nanoparticles are broadened and their oscillator strengths 

  decrease as $\propto n^{-4}$ compared to those in the bulk (decreasing as $\propto n^{-3}$). We explain our
   results by including the effect of quantum confinement of exciton states in the nanoparticles. Our results provide 
  an understanding of the physics of Cu$_2$O Rydberg excitons in confined dimensions.
draft: false
featured: false
image:
  filename: screenshot-2021-06-22-151221.png
  focal_point: Smart
  preview_only: false
date: 2021-06-22T13:26:10.796Z
---
