---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Dynamics of axialized laser-cooled ions in a Penning trap
subtitle: ''
summary: ''
authors:
- E. S. Phillips
- R. J. Hendricks
- A. M. Abdulla
- H. Ohadi
- D. R. Crick
- K. Koo
- D. M. Segal
- R. C. Thompson
tags: []
categories: []
date: '2008-09-01'
lastmod: 2020-12-28T15:15:10Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:53.942307Z'
publication_types:
- '2'
abstract: We report the experimental characterization of axialization—a method of
  reducing the magnetron motion of a small number of ions stored in a Penning trap.
  This is an important step in the investigation of the suitability of Penning traps
  for quantum-information processing. The magnetron motion was coupled to the laser-cooled
  modified cyclotron motion by the application of a near-resonant oscillating quadrupole
  potential (the “axialization drive”). Measurement of cooling rates of the radial
  motions of the ions showed an order-of-magnitude increase in the damping rate of
  the magnetron motion with the axialization drive applied. The experimental results
  are in good qualitative agreement with a recent theoretical study. In particular,
  a classical avoided crossing was observed in the motional frequencies as the axialization
  drive frequency was swept through the optimum value, proving that axialization is
  indeed a resonant effect.
publication: '*Physical Review A*'
url_pdf: http://link.aps.org/doi/10.1103/PhysRevA.78.032307
doi: 10.1103/PhysRevA.78.032307
---
