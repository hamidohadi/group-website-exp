---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Linear Wave Dynamics Explains Observations Attributed to Dark Solitons in a
  Polariton Quantum Fluid
subtitle: ''
summary: ''
authors:
- P. Cilibrizzi
- H. Ohadi
- T. Ostatnicky
- A. Askitopoulos
- W. Langbein
- P. Lagoudakis
tags: []
categories: []
date: '2014-09-01'
lastmod: 2020-12-28T15:15:07Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:53.440329Z'
publication_types:
- '2'
abstract: We investigate the propagation and scattering of polaritons in a planar
  GaAs microcavity in the linear regime under resonant excitation. The propagation
  of the coherent polariton wave across an extended defect creates phase and intensity
  patterns with identical qualitative features previously attributed to dark and half-dark
  solitons of polaritons. We demonstrate that these features are observed for negligible
  nonlinearity (i.e., polariton-polariton interaction) and are, therefore, not sufficient
  to identify dark and half-dark solitons. A linear model based on the Maxwell equations
  is shown to reproduce the experimental observations.
publication: '*Physical Review Letters*'
url_pdf: http://link.aps.org/doi/10.1103/PhysRevLett.113.103901
doi: 10.1103/PhysRevLett.113.103901
---
