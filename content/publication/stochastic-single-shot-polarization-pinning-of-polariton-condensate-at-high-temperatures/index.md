---
title: Stochastic Single-Shot Polarization Pinning of Polariton Condensate at
  High Temperatures
publication_types:
  - "2"
authors:
  - Y. C. Balas
  - E. S. Sedov
  - G. G. Paschos
  - Z. Hatzopoulos
  - H. Ohadi
  - A. V. Kavokin
  - and P. G. Savvidis
doi: 10.1103/PhysRevLett.128.117401
publication: "*Physical Review Letters*"
publication_short: "*Phys. Rev. Lett.*"
abstract: We resolve single-shot polariton condensate polarization dynamics,
  revealing a high degree of circular polarization persistent up to T=170  K.
  The statistical analysis of pulse-to-pulse polariton condensate polarization
  elucidates the stochastic nature of the polarization pinning process, which is
  strongly dependent on the pump laser intensity and polarization. Our
  experiments show that by spatial trapping and isolating condensates from their
  noisy environment it is possible to form strongly spin-polarized polariton
  condensates at high temperatures, offering a promising route to the
  realization of polariton spin lattices for quantum simulations.
draft: false
featured: false
tags:
  - condensation
image:
  filename: featured
  focal_point: Smart
  preview_only: false
date: 2022-03-21T11:27:44.188Z
---
