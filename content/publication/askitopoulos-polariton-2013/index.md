---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Polariton condensation in an optically induced two-dimensional potential
subtitle: ''
summary: ''
authors:
- A. Askitopoulos
- H. Ohadi
- A. V. Kavokin
- Z. Hatzopoulos
- P. G. Savvidis
- P. G. Lagoudakis
tags: []
categories: []
date: '2013-07-01'
lastmod: 2020-12-28T15:15:06Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:54.153937Z'
publication_types:
- '2'
abstract: We demonstrate experimentally the condensation of exciton polaritons through
  optical trapping. The nonresonant pump profile is shaped into a ring and projected
  to a high quality factor microcavity where it forms a two-dimensional repulsive
  optical potential originating from the interactions of polaritons with the excitonic
  reservoir. Increasing the population of particles in the trap eventually leads to
  the emergence of a confined polariton condensate that is spatially decoupled from
  the decoherence inducing reservoir, before any buildup of coherence on the excitation
  region. In a reference experiment, where the trapping mechanism is switched off
  by changing the excitation intensity profile, polariton condensation takes place
  for excitation densities more than two times higher and the resulting condensate
  is subject to much stronger dephasing and depletion processes.
publication: '*Physical Review B*'
url_pdf: http://link.aps.org/doi/10.1103/PhysRevB.88.041308
doi: 10.1103/PhysRevB.88.041308
---
