---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Magneto-optical trapping and background-free imaging for atoms near nanostructured
  surfaces
subtitle: ''
summary: ''
authors:
- Hamid Ohadi
- Matthew Himsworth
- Andre Xuereb
- Tim Freegarde
tags:
- '"Detection"'
- '"Laser cooling"'
- '"Multiphoton processes"'
categories: []
date: '2009-12-01'
lastmod: 2020-12-28T15:15:09Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:53.763485Z'
publication_types:
- '2'
abstract: We demonstrate a combined magneto-optical trap and imaging system that is
  suitable for the investigation of cold atoms near surfaces. In particular, we are
  able to trap atoms close to optically scattering surfaces and to image them with
  an excellent signal-to-noise ratio. We also demonstrate a simple magneto-optical
  atom cloud launching method. We anticipate that this system will be useful for a
  range of experimental studies of novel atom-surface interactions and atom trap miniaturization.
  .
publication: '*Optics Express*'
url_pdf: http://www.opticsexpress.org/abstract.cfm?URI=oe-17-25-23003
doi: 10.1364/OE.17.023003
---
