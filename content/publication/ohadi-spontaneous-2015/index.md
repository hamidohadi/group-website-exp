---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Spontaneous Spin Bifurcations and Ferromagnetic Phase Transitions in a Spinor
  Exciton-Polariton Condensate
subtitle: ''
summary: ''
authors:
- H. Ohadi
- A. Dreismann
- Y. G. Rubo
- F. Pinsker
- Y. del Valle-Inclan Redondo
- S. I. Tsintzos
- Z. Hatzopoulos
- P. G. Savvidis
- J. J. Baumberg
tags: []
categories: []
date: '2015-07-01'
lastmod: 2020-12-28T15:15:10Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:54.485675Z'
publication_types:
- '2'
abstract: We observe a spontaneous parity breaking bifurcation to a ferromagnetic
  state in a spatially trapped exciton-polariton condensate. At a critical bifurcation
  density under nonresonant excitation, the whole condensate spontaneously magnetizes
  and randomly adopts one of two elliptically polarized (up to 95% circularly polarized)
  states with opposite handedness of polarization. The magnetized condensate remains
  stable for many seconds at 5 K, but at higher temperatures, it can flip from one
  magnetic orientation to another. We optically address these states and demonstrate
  the inversion of the magnetic state by resonantly injecting 100-fold weaker pulses
  of opposite spin. Theoretically, these phenomena can be well described as spontaneous
  symmetry breaking of the spin degree of freedom induced by different loss rates
  of the linear polarizations.
publication: '*Physical Review X*'
url_pdf: http://link.aps.org/doi/10.1103/PhysRevX.5.031002
doi: 10.1103/PhysRevX.5.031002
---
