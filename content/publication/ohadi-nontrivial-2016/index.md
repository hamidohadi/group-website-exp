---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Nontrivial Phase Coupling in Polariton Multiplets
subtitle: ''
summary: ''
authors:
- H. Ohadi
- R. L. Gregory
- T. Freegarde
- Y. G. Rubo
- A. V. Kavokin
- N. G. Berloff
- P. G. Lagoudakis
tags: []
categories: []
date: '2016-08-01'
lastmod: 2020-12-28T15:15:09Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:54.646132Z'
publication_types:
- '2'
abstract: We investigate the phase coupling between spatially separated polariton
  condensates under nonresonant optical pulsed excitation. In the simple case of two
  condensates, we observe phase locking either in symmetric or antisymmetric states.
  We demonstrate that the coupling symmetry depends both on the separation distance
  and outflow velocity from the condensates. We interpret the observations through
  stimulated relaxation of polaritons to the phase configuration with the highest
  occupation. We derive an analytic criterion for the phase locking of a pair-polariton
  condensate and extend it to polariton multiplets. In the case of three condensates,
  we predict theoretically and observe experimentally either in-phase locking or the
  appearance of phase winding with phase differences of textpm2$π$/3 between neighbors.
  The latter state corresponds to a vortex of winding number textpm1 across the three
  polariton condensates.
publication: '*Physical Review X*'
url_pdf: http://link.aps.org/doi/10.1103/PhysRevX.6.031032
doi: 10.1103/PhysRevX.6.031032
---
