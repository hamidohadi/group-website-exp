---
# Documentation: https://wowchemy.com/docs/managing-content/

title: A sub-femtojoule electrical spin-switch based on optically trapped polariton
  condensates
subtitle: ''
summary: ''
authors:
- Alexander Dreismann
- Hamid Ohadi
- Yago del Valle-Inclan Redondo
- Ryan Balili
- Yuri G. Rubo
- Simeon I. Tsintzos
- George Deligeorgis
- Zacharias Hatzopoulos
- Pavlos G. Savvidis
- Jeremy J. Baumberg
tags:
- '"Nonlinear optics"'
- '"Quantum information"'
- '"Nanophotonics and plasmonics"'
- '"Bose–Einstein condensates"'
- '"Optoelectronic devices and components"'
categories: []
date: '2016-10-01'
lastmod: 2020-12-28T15:15:08Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:55.369611Z'
publication_types:
- '2'
abstract: Practical challenges to extrapolating Mooretextquoterights law favour alternatives
  to electrons as information carriers. Two promising candidates are spin-based and
  all-optical architectures, the former offering lower energy consumption, the latter
  superior signal transfer down to the level of chip-interconnects. Polaritons—spinor
  quasi-particles composed of semiconductor excitons and microcavity photons—directly
  couple exciton spins and photon polarizations, combining the advantages of both
  approaches. However, their implementation for spintronics has been hindered because
  polariton spins can be manipulated only optically or by strong magnetic fields.
  Here we use an external electric field to directly control the spin of a polariton
  condensate, bias-tuning the emission polarization. The nonlinear spin dynamics offers
  an alternative route to switching, allowing us to realize an electrical spin-switch
  exhibiting ultralow switching energies below 0.5 fJ. Our results lay the foundation
  for development of devices based on the electro-optical control of coherent spin
  ensembles on a chip.
publication: '*Nature Materials*'
url_pdf: http://www.nature.com/nmat/journal/v15/n10/full/nmat4722.html
doi: 10.1038/nmat4722
---
