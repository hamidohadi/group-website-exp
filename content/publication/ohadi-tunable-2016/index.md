---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Tunable Magnetic Alignment between Trapped Exciton-Polariton Condensates
subtitle: ''
summary: ''
authors:
- H. Ohadi
- Y. del Valle-Inclan Redondo
- A. Dreismann
- Y. G. Rubo
- F. Pinsker
- S. I. Tsintzos
- Z. Hatzopoulos
- P. G. Savvidis
- J. J. Baumberg
tags: []
categories: []
date: '2016-03-01'
lastmod: 2020-12-28T15:15:10Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:54.326053Z'
publication_types:
- '2'
abstract: Tunable spin correlations are found to arise between two neighboring trapped
  exciton-polariton condensates which spin polarize spontaneously. We observe a crossover
  from an antiferromagnetic to a ferromagnetic pair state by reducing the coupling
  barrier in real time using control of the imprinted pattern of pump light. Fast
  optical switching of both condensates is then achieved by resonantly but weakly
  triggering only a single condensate. These effects can be explained as the competition
  between spin bifurcations and spin-preserving Josephson coupling between the two
  condensates, and open the way to polariton Bose-Hubbard ladders.
publication: '*Physical Review Letters*'
url_pdf: http://link.aps.org/doi/10.1103/PhysRevLett.116.106403
doi: 10.1103/PhysRevLett.116.106403
---
