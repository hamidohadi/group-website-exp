---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Polariton spin whirls
subtitle: ''
summary: ''
authors:
- P. Cilibrizzi
- H. Sigurdsson
- T. C. H. Liew
- H. Ohadi
- S. Wilkinson
- A. Askitopoulos
- I. A. Shelykh
- P. G. Lagoudakis
tags: []
categories: []
date: '2015-10-01'
lastmod: 2020-12-28T15:15:07Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:52.881108Z'
publication_types:
- '2'
abstract: We report on the observation of spin whirls in a radially expanding polariton
  condensate formed under nonresonant optical excitation. Real space imaging of polarization-
  and time-resolved photoluminescence reveals a spiralling polarization pattern in
  the plane of the microcavity. Simulations of the spatiotemporal dynamics of a spinor
  condensate reveal the crucial role of polariton interactions with a spinor exciton
  reservoir. Harnessing spin-dependent interactions between the exciton reservoir
  and polariton condensates allows for the manipulation of spin currents and the realization
  of dynamic collective spin effects in solid-state systems.
publication: '*Physical Review B*'
url_pdf: http://link.aps.org/doi/10.1103/PhysRevB.92.155308
doi: 10.1103/PhysRevB.92.155308
---
