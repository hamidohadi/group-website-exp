---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Nonlinear Optical Spin Hall Effect and Long-Range Spin Transport in Polariton
  Lasers
subtitle: ''
summary: ''
authors:
- E. Kammann
- T. C. H. Liew
- H. Ohadi
- P. Cilibrizzi
- P. Tsotsis
- Z. Hatzopoulos
- P. G. Savvidis
- A. V. Kavokin
- P. G. Lagoudakis
tags: []
categories: []
date: '2012-07-01'
lastmod: 2020-12-28T15:15:09Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:52.721920Z'
publication_types:
- '2'
abstract: We report on the experimental observation of the nonlinear analogue of the
  optical spin Hall effect under highly nonresonant circularly polarized excitation
  of an exciton-polariton condensate in a GaAs/AlGaAs microcavity. The circularly
  polarized polariton condensates propagate over macroscopic distances, while the
  collective condensate spins coherently precess around an effective magnetic field
  in the sample plane performing up to four complete revolutions.
publication: '*Physical Review Letters*'
url_pdf: http://link.aps.org/doi/10.1103/PhysRevLett.109.036404
doi: 10.1103/PhysRevLett.109.036404
---
