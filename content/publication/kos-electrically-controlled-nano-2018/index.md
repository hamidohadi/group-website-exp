---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Electrically Controlled Nano and Micro Actuation in Memristive Switching Devices
  with On-Chip Gas Encapsulation
subtitle: ''
summary: ''
authors:
- Dean Kos
- Hippolyte P. A. G. Astier
- Giuliana Di Martino
- Jan Mertens
- Hamid Ohadi
- Domenico De Fazio
- Duhee Yoon
- Zhuang Zhao
- Alexander Kuhn
- Andrea C. Ferrari
- Christopher J. B. Ford
- Jeremy J. Baumberg
tags:
- '"graphene"'
- '"nanoactuation"'
- '"nanoparticles"'
- '"plasmonic coupling"'
- '"resistive switching"'
categories: []
date: '2018-01-01'
lastmod: 2020-12-28T15:15:09Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:56.763123Z'
publication_types:
- '2'
abstract: Nanoactuators are a key component for developing nanomachinery. Here, an
  electrically driven device yielding actuation stresses exceeding 1 MPa withintegrated
  optical readout is demonstrated. 10 nm thick Al2O3 electrolyte films are sandwiched
  between graphene and Au electrodes. These allow reversible room-temperature solid-state
  redox reactions, producing Al metal and O2 gas in a memristive-type switching device.
  The resulting high-pressure oxygen micro-fuel reservoirs are encapsulated under
  the graphene, swelling to heights of up to 1 textmum, which can be dynamically tracked
  by plasmonic rulers. Unlike standard memristors where the memristive redox reaction
  occurs in single or few conductive filaments, the mechanical deformation forces
  the creation of new filaments over the whole area of the inflated film. The resulting
  on–off resistance ratios reach 108 in some cycles. The synchronization of nanoactuation
  and memristive switching in these devices is compatible with large-scale fabrication
  and has potential for precise and electrically monitored actuation technology.
publication: '*Small*'
url_pdf: https://onlinelibrary.wiley.com/doi/abs/10.1002/smll.201801599
doi: 10.1002/smll.201801599
---
