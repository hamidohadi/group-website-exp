---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Spin Order and Phase Transitions in Chains of Polariton Condensates
subtitle: ''
summary: ''
authors:
- H. Ohadi
- A. J. Ramsay
- H. Sigurdsson
- Y. del Valle-Inclan Redondo
- S. I. Tsintzos
- Z. Hatzopoulos
- T. C. H. Liew
- I. A. Shelykh
- Y. G. Rubo
- P. G. Savvidis
- J. J. Baumberg
tags: []
categories: []
date: '2017-08-01'
lastmod: 2020-12-28T15:15:09Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:55.701887Z'
publication_types:
- '2'
abstract: We demonstrate that multiply coupled spinor polariton condensates can be
  optically tuned through a sequence of spin-ordered phases by changing the coupling
  strength between nearest neighbors. For closed four-condensate chains these phases
  span from ferromagnetic (FM) to antiferromagnetic (AFM), separated by an unexpected
  crossover phase. This crossover phase is composed of alternating FM-AFM bonds. For
  larger eight-condensate chains, we show the critical role of spatial inhomogeneities
  and demonstrate a scheme to overcome them and prepare any desired spin state. Our
  observations thus demonstrate a fully controllable nonequilibrium spin lattice.
publication: '*Physical Review Letters*'
url_pdf: https://link.aps.org/doi/10.1103/PhysRevLett.119.067401
doi: 10.1103/PhysRevLett.119.067401
---
