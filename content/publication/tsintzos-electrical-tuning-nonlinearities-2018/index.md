---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Electrical Tuning of Nonlinearities in Exciton-Polariton Condensates
subtitle: ''
summary: ''
authors:
- S. I. Tsintzos
- A. Tzimis
- G. Stavrinidis
- A. Trifonov
- Z. Hatzopoulos
- J. J. Baumberg
- H. Ohadi
- P. G. Savvidis
tags: []
categories: []
date: '2018-07-01'
lastmod: 2020-12-28T15:15:11Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:56.600047Z'
publication_types:
- '2'
abstract: A primary limitation of the intensively researched polaritonic systems compared
  to their atomic counterparts for the study of strongly correlated phenomena and
  many-body physics is their relatively weak two-particle interactions compared to
  disorder. Here, we show how new opportunities to enhance such on-site interactions
  and nonlinearities arise by tuning the exciton-polariton dipole moment in electrically
  biased semiconductor microcavities incorporating wide quantum wells. The applied
  field results in a twofold enhancement of exciton-exciton interactions as well as
  more efficiently driving relaxation towards low energy polariton states, thus, reducing
  condensation threshold.
publication: '*Physical Review Letters*'
url_pdf: https://link.aps.org/doi/10.1103/PhysRevLett.121.037401
doi: 10.1103/PhysRevLett.121.037401
---
