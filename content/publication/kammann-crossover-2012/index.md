---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Crossover from photon to exciton-polariton lasing
subtitle: ''
summary: ''
authors:
- Elena Kammann
- Hamid Ohadi
- Maria Maragkou
- Alexey V. Kavokin
- Pavlos G. Lagoudakis
tags: []
categories: []
date: '2012-10-01'
lastmod: 2020-12-28T15:15:08Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:52.241666Z'
publication_types:
- '2'
abstract: We report on a real-time observation of the crossover between photon and
  exciton-polariton lasing in a semiconductor microcavity. Both lasing phases are
  observed at different times after a high-power excitation pulse. Energy-, time-
  and angle-resolved measurements allow for the transient characterization of carrier
  distribution and effective temperature. We find signatures of Bose–Einstein condensation,
  namely macroscoping occupation of the ground state and narrowing of the linewidth
  in both lasing regimes. The Bernard–Douraffourgh condition for inversion was tested
  and the polariton laser as well as the photon laser under continuous wave excitation
  were found to operate at estimated densities below the theoretically predicted inversion
  threshold.
publication: '*New Journal of Physics*'
url_pdf: http://iopscience.iop.org/1367-2630/14/10/105003
doi: 10.1088/1367-2630/14/10/105003
---
