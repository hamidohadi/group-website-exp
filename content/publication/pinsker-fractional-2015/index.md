---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Fractional quantum mechanics in polariton condensates with velocity-dependent
  mass
subtitle: ''
summary: ''
authors:
- F. Pinsker
- W. Bao
- Y. Zhang
- H. Ohadi
- A. Dreismann
- J. J. Baumberg
tags: []
categories: []
date: '2015-11-01'
lastmod: 2020-12-28T15:15:10Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:53.095776Z'
publication_types:
- '2'
abstract: We introduce and analyze a mean-field model for polariton condensates which
  includes a velocity dependence of the effective polariton mass due to the photon
  and exciton components. The effective mass depends on the in-plane wave vector k,
  which at the inflection point of the lower polariton energy branch becomes infinite,
  and above this becomes negative. The polariton condensate modes of this mean-field
  theory are now sensitive to mass variations and, for certain points of the energy
  dispersion, the polariton condensate mode represents fractional quantum mechanics.
  The impact of the generalized kinetic-energy term is elucidated by numerical studies
  in two dimensions showing significant differences for large velocities. Analytical
  expressions for plane-wave solutions as well as a linear waves analysis show the
  significance of this model.
publication: '*Physical Review B*'
url_pdf: http://link.aps.org/doi/10.1103/PhysRevB.92.195310
doi: 10.1103/PhysRevB.92.195310
---
