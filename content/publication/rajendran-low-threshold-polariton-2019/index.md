---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Low Threshold Polariton Lasing from a Solution-Processed Organic Semiconductor
  in a Planar Microcavity
subtitle: ''
summary: ''
authors:
- Sai Kiran Rajendran
- Mengjie Wei
- Hamid Ohadi
- Arvydas Ruseckas
- Graham A. Turnbull
- Ifor D. W. Samuel
tags:
- '"polariton lasing"'
- '"low threshold"'
- '"microcavities"'
- '"organic semiconductors"'
- '"strong coupling"'
categories: []
date: '2019-01-01'
lastmod: 2020-12-28T15:15:10Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:56.960980Z'
publication_types:
- '2'
abstract: Organic semiconductor materials are widely studied for light emission and
  lasing due to their ability to tune the emission wavelength through chemical structural
  modification and their relative ease of fabrication. Strong light–matter coupling
  is a promising route toward a coherent light source because it has the potential
  for polariton lasing without population inversion. However, the materials studied
  so far have relatively high thresholds for polariton lasing. Here, the suitability
  of pentafluorene for strong coupling and low threshold polariton lasing is reported.
  A protective buffer layer is used to reduce degradation during fabrication and the
  lasing threshold is lowered using negative detuning to maximize radiative decay.
  A low threshold of 17 textmuJ cm-2, corresponding to an absorbed energy density
  of 11.7 textmuJ cm-2, is obtained. This study shows that pentafluorene is an attractive
  material for polariton lasing and will assist in the development of low threshold
  electrically pumped lasing from polariton devices.
publication: '*Advanced Optical Materials*'
url_pdf: https://onlinelibrary.wiley.com/doi/abs/10.1002/adom.201801791
doi: 10.1002/adom.201801791
---
