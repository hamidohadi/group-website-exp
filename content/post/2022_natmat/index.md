---
title: Publication news
date: 2022-05-01T10:41:31.666Z
summary: Excited to announce that our Rydberg polariton work is now available online in [Nature Materials](https://rdcu.be/cLlPg). See also our university [press release](https://news.st-andrews.ac.uk/archive/ancient-namibian-stone-holds-key-to-future-quantum-computers/), and Nature Mateirals [news and views](https://t.co/XjwJPoUGFN)
draft: false
featured: true
image:
  filename: featured
  focal_point: Smart
  preview_only: false

tags:
- news

categories:
- News
---
Excited to announce that our Rydberg polariton work is now available online in [Nature Materials](https://rdcu.be/cLlPg). See also our university [press release](https://news.st-andrews.ac.uk/archive/ancient-namibian-stone-holds-key-to-future-quantum-computers/), and Nature Mateirals [news and views](https://t.co/XjwJPoUGFN).  <!--more--> Congratulations to [Kostas]({{< ref "/authors/kostas" >}}) and [Sai]({{< ref "/authors/sai" >}}), and thanks to our collaborators in Harvard, Aarhus, and Macquarie.