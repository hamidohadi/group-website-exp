---
title: PhD position for 2018 academic year
subtitle: 
# Summary for listings and search engines
summary: We have 2 fully funded (covering tuition fees and living expenses) PhD position for 2018

# Link this post with a project
projects: []

# Date published
date: "2018-01-14T00:00:00Z"

# Date updated
lastmod: "2018-01-14T00:00:00Z"

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
# image:
#   caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/CpkOjOcXdUY)'
#   focal_point: ""
#   placement: 2
#   preview_only: false

authors:
- admin

tags:
- PhD positions

categories:
- Jobs
---

We have two fully funded (covering tuition fees and living expenses) PhD positions for this
academic year: one position in
[Spin lattices](/project/spin-lattices) and another in
[Polaritonic devices](/project/polariton-devices).

Please note [EPSRC
restrictions](https://www.epsrc.ac.uk/skills/students/help/eligibility) apply. To qualify you must:

 * have been resident in the UK for at least three years
 * have no restrictions on how long you can stay in the UK.


Chinese nationals can apply through the [China Scholarship Council - University
of St Andrews
Scholarships](https://www.st-andrews.ac.uk/study/apply/international/csc).

Exceptional European candidates may still be eligible and are encouraged to enquire.
Exceptional international candidates may be eligible for a [SUPA prize](https://apply.supa.ac.uk/) and are encouraged to enquire.


Interested candidates should quickly send their CV to [Dr Ohadi](mailto:ho35@st-andrews.ac.uk).
