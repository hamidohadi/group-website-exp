---
title: Polariton Lattices
summary: Interacting lattices of exciton-polariton condensates
tags:
- exciton-polariton
- spin lattice
- frustration
date: "2018-11-27T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

# image:
#   caption: Photo by rawpixel on Unsplash
#   focal_point: Smart

# links:
# - icon: twitter
#   icon_pack: fab
#   name: Follow
#   url: https://twitter.com/georgecushen
# url_code: ""
# url_pdf: ""
# url_slides: ""
# url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
# slides: example
---

## Introduction

One of the most interesting concepts in condensed matter is the so-called
“frustration”, where a few spins in the system cannot find an orientation to
fully satisfy all the interactions with its neighbouring spins. Frustration is a
complex and not well-understood phenomenon with interdisciplinary applications
to artificial neural networks as well as material science. The classic example
is 3 spins a triangular geometry with antiferromagnetic coupling between the
nearest neighbours. We have recently introduced exciton-polariton (polariton)
condensates as a new platform to study such spin interactions [1-3]. Polariton
condensates are macroscopic quantum states with picosecond dynamics and unique
spin properties arising from their nonlinearities.  

## Aims

We experimentally study the spin properties of coupled polariton
condensates in 2-dimensional optical lattices in various geometries,
specifically the phenomenon of frustration. This project involves a
fair amount of programming, and numerical simulations in a collaboration with
our theorist collaborators.

## Industrial links?

We have an ongoing industrial partner at [Hitachi, Cambridge](http://www.hit.phy.cam.ac.uk/).

## Interested to work on this project?

Send your CV and cover letter to [Dr Ohadi](mailto:ho35@st-andrews.ac.uk).
