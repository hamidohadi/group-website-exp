---
title: Giant Rydberg polaritons
date: 2018-11-27T00:00:00Z
summary: Strong light-matter coupling of Rydberg excitons in cuprous oxide
tags:
  - Strong light-matter coupling
  - Exciton-polariton
  - Rydberg exciton
  - Cuprous oxide
math: true
image:
  filename: Featured.png
---



## Introduction

Exploiting the laws of quantum mechanics for the benefit of humanity in the so-called "second quantum revolution" is one of the greatest challenges of the 21st century. For this we need to efficiently produce particles, control their states, detect them and make them interact strongly at the single-particle level. Photons, the quantum particles of light, are one of the most promising candidates. We can easily detect and control their states and we can efficiently produce them individually. However, making them interact strongly to build a large quantum network is a notoriously difficult task because photons do not interact at low energies. To make them interact indirectly, one can hybridise them with other massive particles that strongly interact and form quasiparticles called 'polaritons'.


## Aims

In this project, we aim to hybridise photons with Rydberg excitons [1]. Rydberg excitons are highly excited (principal quantum number $n\sim20$) electron-hole pairs that can span macroscopic dimensions. Because of their macroscopic dimensions they strongly repel. The semiconductor device that we have chosen for hybridisation is a 2-dimensional semiconductor microcavity formed by two highly reflective mirrors encapsulating a cuprous oxide microcrystals and thin film. Photons confined in the microcavity strongly couple to Rydberg excitons in cuprous oxide to form Rydberg polaritons. This will allow us to explore quantum optics at the single-particle limit and form 2-dimensional networks of strongly correlated photons for future quantum simulators.

[1] Kazimierczuk et al, [Nature 514, 343 (2014)](https://www.nature.com/articles/nature13832)

## Industrial link

We have an ongoing industrial partner at [Hitachi, Cambridge](http://www.hit.phy.cam.ac.uk/).

## Interested to work on this project?

Send your CV and cover letter to [Dr Ohadi](mailto:ho35@st-andrews.ac.uk).
