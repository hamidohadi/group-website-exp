---
title: Perovskite polaritons
summary: Room-temperature single polariton nonlinearity in 2-dimensional materials
tags:
- Exciton-polariton
- Condensation
- Spin devices
- Organics
date: "2018-11-27T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

# image:
#   caption: Photo by rawpixel on Unsplash
#   focal_point: Smart

# links:
# - icon: twitter
#   icon_pack: fab
#   name: Follow
#   url: https://twitter.com/FluidsOfLight
# url_code: ""
# url_pdf: ""
# url_slides: ""
# url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
# slides: example
---
## Introduction

2D semiconductors are currently the most important class of materials to be exploited for next generation nanophotonics. Among these, hybrid perovskites have shown immense potential in terms of bandgap tunability, room temperature operation, easy synthesis, and scalability.1 Perovskites are special types of inorganic materials with the chemical composition ABX3, but when the ‘A’ cation gets too big, the arrangement of atoms changes from a 3-dimensional structure to a 2-dimensional structure. Hybrid perovskites consist of an organic and an inorganic component, making them highly tuneable. As detailed below, we aim to significantly enhance particle repulsion (nonlinearities) in two-dimensional perovskite nanosheets (2DPNs) by forming “dipolaritons”. Dipolaritons are indirect excitons strongly coupled to microcavity photons. Their tunable enhanced interaction make them useful for the next generation nonlinear nanophotonic devices such as optical gates, transistors, and switches, which are crucial for our next generation sustainable computation and telecommunications.

Perovskites can be synthesised easily and in large scale into hybrid crystals which contain organic molecular spacer layers and inorganic layers. In 2DPNs excitons (bound electron-hole pairs) of high binding energies are formed even at  room temperature and confined to the inorganic layers by the organic spacer. This enables 2DPNs to be strong light absorbing and emitting materials. Microcavities are optical structures that trap light (or photons) inside as they are made of two mirrors separated by a few micrometres. 2DPNs can also be strongly coupled to light in microcavities to form polaritons, hybridised excitons and photons, which possess the interaction strength of excitons (repulsion) and fast dynamics (speed) of photons. In indirect excitons the electron and hole form in different layers with the organic group acting as a tunnelling barrier. Because of this separation, the electron-hole dipole interaction and their tunability by external electric fields is significantly enhanced.

## Aims

Our aim is to exploit the physics of optical excitation in new 2-dimensional perovskite nanosheets (2DPNs) when exposed to an electric field. Under electric fields, 2DPNs when coupled to light form new particles called dipolaritons. Dipolaritons are highly interacting particles due to their extremely polarised nature. Here, we aim to demonstrate and exploit the huge interaction strength of these dipolaritons by fabricating new photonic devices in School of Physics encapsulating newly synthesised perovskites in School of Chemistry to be used for future room temperature quantum applications.

## Interested to work on this project?

Send your CV and cover letter to [Dr Ohadi](mailto:ho35@st-andrews.ac.uk).
