---
# Display name
title: Dr Hamid Ohadi

# Is this the primary user of the site?
superuser: true

user_groups: ["Principal Investigator"]

# Role/position/tagline
role: Lecturer

# Organizations/Affiliations to show in About widget
# organizations:
# - name: Stanford University
#   url: https://www.stanford.edu/

# Short bio (displayed in user profile at end of posts)
# bio: My research interests include distributed robotics, mobile computing and programmable matter.

# Interests to show in About widget
interests:
- Nano Photonics
- Light-matter interaction
- Quantum Technologies


# Education to show in About widget
education:
  courses:
  - course: PhD in Physics
    institution: Imperial College London
    year: 2008
  - course: BSc in Applied Physics
    institution: Sharif University of Technology
    year: 2004

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:ho35@st-andrews.ac.uk'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/FluidsOfLight
  label: Follow us on Twitter
  display:
    header: true
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.co.uk/citations?user=x6E7md4AAAAJ
- icon: orcid
  icon_pack: ai
  link: https://orcid.org/0000-0001-6418-111X
# - icon: graduation-cap  # Alternatively, use `google-scholar` icon from `ai` icon pack
#   icon_pack: fas
#   link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
# - icon: github
#   icon_pack: fab
#   link: https://github.com/gcushen
# - icon: linkedin
#   icon_pack: fab
#   link: https://www.linkedin.com/

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "ho35@st-andrews.ac.uk"

# Highlight the author in author lists? (true/false)
highlight_name: false
---

I am a lecturer in physics at the University of St
Andrews. My interests fall into the broad scheme of photonics and
quantum science, specifically, light-matter interaction in microcavities and nonlinearities at the single-particle limit. I lead the Quantum Light Matter group, which
currently look at *strong light-matter interaction* in novel materials and
exploiting it for new *quantum technologies*.

<!-- {{< icon name="download" pack="fas" >}} Download my {{< staticref "media/demo_resume.pdf" "newtab" >}}resumé{{< /staticref >}}. -->
