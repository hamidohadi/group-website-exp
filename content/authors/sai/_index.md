---
# Display name
title: Dr Sai Kiran Rajendran

# Is this the primary user of the site?
superuser: false

user_groups: ["Post-doctoral Researchers"]

# Role/position/tagline
role: PDRA

# Organizations/Affiliations to show in About widget
# organizations:
# - name: Stanford University
#   url: https://www.stanford.edu/

# Short bio (displayed in user profile at end of posts)
bio: My research interests include distributed robotics, mobile computing and programmable matter.

# Interests to show in About widget
interests:
- Nanophotonics
- Spectroscopy
- Organic
- Light-matter coupling
- Rydberg excitons


# Education to show in About widget
education:
  courses:
  - course: PhD in Physics
    institution: Politecnico di Milano
    year: 2014
  - course: MA in Physics (Specialisation in Photonics)
    institution: Sri Sathya Sai University
    year: 2009
  - course: BSc (Hons) in Physics
    institution: Sri Sathya Sai University
    year: 2007



# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:skr7@st-andrews.ac.uk'
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?hl=en&user=mAztOK8AAAAJ
- icon: orcid
  icon_pack: ai
  link: https://orcid.org/0000-0002-9079-120X
# - icon: graduation-cap  # Alternatively, use `google-scholar` icon from `ai` icon pack
#   icon_pack: fas
#   link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
# - icon: github
#   icon_pack: fab
#   link: https://github.com/gcushen
# - icon: linkedin
#   icon_pack: fab
#   link: https://www.linkedin.com/

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "skr7@st-andrews.ac.uk"

# Highlight the author in author lists? (true/false)
highlight_name: false
---

I am a research fellow at the School of Physics and Astronomy, University of St Andrews, UK, working on the EPSRC Project Rydberg Polaritons in Cu2O microcavities ([EP/S014403/1](https://gow.epsrc.ukri.org/NGBOViewGrant.aspx?GrantRef=EP/S014403/1)) since November 2019 with Dr. Hamid Ohadi at the Quantum Fluids of Light group. During the earlier period from November 2015 to 2019, I worked on the EPSRC project Hybrid Polaritonics ([EP/M025330/1](https://gow.epsrc.ukri.org/NGBOViewGrant.aspx?GrantRef=EP/M025330/1)) with Prof. Ifor Samuel and Prof. Graham Turnbull at the [Organic Semiconductor Centre](https://www.st-andrews.ac.uk/~osc/home.shtml). In 2015, I worked at the Ultrafast spectroscopy group with Dr. Tersilla Virgili and Prof. Giulio Cerulllo at [Politecnico di Milano](https://www.fisi.polimi.it/en/research/research_structures/research_lines/50399). 
Earlier I graduated in 2009 with Masters in Physics from [Sri Sathya Sai Institute of Higher Learning](http://sssihl.edu.in/sssuniversity/Research/DepartmentofPhysics/ResearchAreas.aspx), Anantapur, India. I then joined as an early stage researcher at the [Dipartimento di Fisica, Politecnico di Milano](https://www.fisi.polimi.it/en/research/research_structures/research_lines/50399), Milan, Italy as part of the Marie Curie ITN-FP7-PEOPLE Project ICARUS ([237900](https://cordis.europa.eu/project/id/237900)) and obtained a Ph.D. degree in 2014.

My current interests involve fabrication and engineering semiconductor and plasmonic materials towards understanding their properties using a wide range of spectroscopic studies for applications in optoelectronics. Materials of interest are carbon nanotubes, inorganic semiconductor and plasmonic nanoparticles, liquid crystals, perovskites, and a wide range of organic semiconductor materials. Fabrication techniques of interest are thermal evaporation, RF magnetron sputtering, spin coating, chemical vapour deposition and focused ion beam etching. Spectroscopy techniques of interest are ellipsometry, Raman spectroscopy, scanning electron microscopy, photoluminescence and amplified spontaneous emission, time-correlated single photon counting, streak camera, X-ray diffraction, Fourier imaging spectroscopy, interferometry, cryogenic spectroscopy, femtosecond transient absorption and 2D electronic spectroscopy.


ORCID ID: https://orcid.org/0000-0002-9079-120X

Some key publications:

* [Low Threshold Polariton Lasing from a Solution‐Processed Organic Semiconductor in a Planar Microcavity](https:///doi.org/10.1002/adom.201801791), Adv. Opt. Mat. (2019)
* [Lévy Defects in Matrix-Immobilized J Aggregates: Tracing Intra-and Intersegmental Exciton Relaxation](https://doi.org/10.1021/acs.jpclett.6b02704), JPCL (2017) 
* [Direct evidence of Rabi oscillations and antiresonance in a strongly coupled organic microcavity](https://doi.org/10.1103/PhysRevB.91.201305), PRB (2015)
