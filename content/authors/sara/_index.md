---
# Display name
title: Sara Henda

# Is this the primary user of the site?
superuser: false

user_groups: ["PhD Students"]

# Role/position/tagline
role: Postgraduate Student

# Organizations/Affiliations to show in About widget
# organizations:
# - name: Stanford University
#   url: https://www.stanford.edu/

# Short bio (displayed in user profile at end of posts)
bio: 

# Interests to show in About widget
interests:
- Nanophotonics
- Spectroscopy
- 2D Perovskites


# Education to show in About widget
education:
  courses:
  - course: MSc in Applied Physics
    institution: University ofStrathclyde
    year: 2020
  - course: BSc in Physics
    institution: University of Algiers 1 
    year: 2018


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:sh363@st-andrews.ac.uk'
# - icon: google-scholar
#   icon_pack: ai
#   link: https://scholar.google.com/citations?hl=en&user=mAztOK8AAAAJ
# - icon: orcid
#   icon_pack: ai
#   link: https://orcid.org/0000-0002-9079-120X
# - icon: graduation-cap  # Alternatively, use `google-scholar` icon from `ai` icon pack
#   icon_pack: fas
#   link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
# - icon: github
#   icon_pack: fab
#   link: https://github.com/gcushen
# - icon: linkedin
#   icon_pack: fab
#   link: https://www.linkedin.com/

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "sh363@st-andrews.ac.uk"

# Highlight the author in author lists? (true/false)
highlight_name: false
---
Sara is a "Worldleading St Andrews" PhD student between our group and [Dr Julia Payne](https://jlpgroup.wp.st-andrews.ac.uk/) in Chemistry. Sara's project is on polaritons in 2D hybrid perovskites.
