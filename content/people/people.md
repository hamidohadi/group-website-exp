---
# An instance of the People widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: people

# This file represents a page section.
headless: false

# Order that this section appears on the page.
weight: 100

title: The Team
subtitle:

content:
  # Choose which groups/teams of users to display.
  #   Edit `user_groups` in each user's profile to add them to one or more of these groups.
  user_groups:
  - Principal Investigator
  - Post-doctoral Researchers
  - PhD Students
  - Project Students
  - Administration
  - Visitors
  - Alumni
design:
  show_interests: false
  show_role: false
  show_social: true
---

## Project Students
* Yiu Chiu (Summer internship, 2022)
  
## Alumni
* Ben Murphy (MPhys project, 2022)
* Danica Woolgar (BSc project, 2021)
* Rucheng Yao (BSc, project, 2021)
* Tomasz Plaskocinski (MPhys project, 2020)
* Daniel Gorman (BSc project, 2020)
* Alastair Clarke (MPhys project, 2019)
* Hamish Ballantyne (Summer project, 2019)
* Matthew Gerard (Summer project, 2019)
* Felix Scott (Summer project, 2018-2019)
* Scott Stevenson (Summer project, 2018)